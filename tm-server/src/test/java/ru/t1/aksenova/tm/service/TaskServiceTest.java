package ru.t1.aksenova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.service.*;
import ru.t1.aksenova.tm.comparator.NameComparator;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.exception.AbstractException;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.model.Task;
import ru.t1.aksenova.tm.model.User;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static ru.t1.aksenova.tm.constant.TaskTestData.*;
import static ru.t1.aksenova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class TaskServiceTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private static final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private static final IUserService userService = new UserService(propertyService, connectionService, taskService, projectService);

    @NotNull
    private static String userId = "";

    @NotNull
    private static String adminId = "";

    @BeforeClass
    public static void initData() {
        @NotNull final User user = userService.add(USER_TEST);
        userId = user.getId();
        @NotNull final User admin = userService.add(ADMIN_TEST);
        adminId = admin.getId();
        USER_TASK1.setUserId(userId);
        USER_TASK2.setUserId(userId);
        ADMIN_TASK1.setUserId(adminId);
        ADMIN_TASK2.setUserId(adminId);
    }

    @AfterClass
    public static void clearData() {
        @Nullable User user = userService.findOneById(userId);
        if (user != null) userService.remove(user);
        user = userService.findOneById(adminId);
        if (user != null) userService.remove(user);
    }

    @Before
    public void before() {
        taskService.add(USER_TASK1);
        taskService.add(USER_TASK2);
    }

    @After
    public void after() {
        taskService.removeAll(userId);
        taskService.removeAll(adminId);
    }

    @Test
    public void add() {
        Assert.assertThrows(AbstractException.class, () -> taskService.add(NULL_TASK));
        Assert.assertNotNull(taskService.add(ADMIN_TASK1));
        @Nullable final Task task = taskService.findOneById(adminId, ADMIN_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(ADMIN_TASK1.getId(), task.getId());
    }

    @Test
    public void addMany() {
        Assert.assertNotNull(taskService.set(ADMIN_TASK_LIST));
        for (final Task task : ADMIN_TASK_LIST)
            Assert.assertEquals(task.getId(), taskService.findOneById(adminId, task.getId()).getId());
    }

    @Test
    public void addByUserId() {
        Assert.assertThrows(AbstractException.class, () -> taskService.add(null, ADMIN_TASK1));
        Assert.assertThrows(AbstractException.class, () -> taskService.add("", ADMIN_TASK1));
        Assert.assertThrows(AbstractException.class, () -> taskService.add(adminId, NULL_TASK));
        Assert.assertNotNull(taskService.add(adminId, ADMIN_TASK1));
        @Nullable final Task task = taskService.findOneById(adminId, ADMIN_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(ADMIN_TASK1.getId(), task.getId());
    }

    @Test
    public void createByUserId() {
        Assert.assertThrows(AbstractException.class, () -> taskService.create(null, ADMIN_TASK1.getName(), ADMIN_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> taskService.create(adminId, null, ADMIN_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> taskService.create(adminId, ADMIN_TASK1.getName(), null));
        @NotNull final Task task = taskService.create(adminId, ADMIN_TASK1.getName(), ADMIN_TASK1.getDescription());
        Assert.assertNotNull(task);
        Assert.assertEquals(ADMIN_TASK1.getName(), task.getName());
        Assert.assertEquals(ADMIN_TASK1.getDescription(), task.getDescription());
        Assert.assertEquals(adminId, task.getUserId());
    }

    @Test
    public void updateByUserIdById() {
        Assert.assertThrows(AbstractException.class, () -> taskService.updateById(null, USER_TASK1.getId(), USER_TASK1.getName(), USER_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> taskService.updateById(userId, null, USER_TASK1.getName(), USER_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> taskService.updateById(userId, NON_EXISTING_TASK_ID, USER_TASK1.getName(), USER_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> taskService.updateById(userId, USER_TASK1.getId(), null, USER_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> taskService.updateById(userId, USER_TASK1.getId(), USER_TASK1.getName(), null));
        @NotNull final Task task = taskService.updateById(userId, USER_TASK1.getId(), TASK_NAME, TASK_DESCR);
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK_NAME, task.getName());
        Assert.assertEquals(TASK_DESCR, task.getDescription());
        Assert.assertEquals(userId, task.getUserId());
    }

    @Test
    public void updateByUserIdByIndex() {
        @NotNull final List<Task> tasks = taskService.findAll(userId);
        final int index = getIndexFromList(tasks, USER_TASK1.getId());
        Assert.assertThrows(AbstractException.class, () -> taskService.updateByIndex(null, index, USER_TASK1.getName(), USER_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> taskService.updateByIndex(userId, null, USER_TASK1.getName(), USER_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> taskService.updateByIndex(userId, -1, USER_TASK1.getName(), USER_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> taskService.updateByIndex(userId, index, null, USER_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> taskService.updateByIndex(userId, index, USER_TASK1.getName(), null));
        @NotNull final Task task = taskService.updateByIndex(userId, index, TASK_NAME, TASK_DESCR);
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK_NAME, task.getName());
        Assert.assertEquals(TASK_DESCR, task.getDescription());
        Assert.assertEquals(userId, task.getUserId());
    }

    @Test
    public void changeProjectStatusById() {
        Assert.assertThrows(AbstractException.class, () -> taskService.changeTaskStatusById(null, USER_TASK1.getId(), Status.IN_PROGRESS));
        Assert.assertThrows(AbstractException.class, () -> taskService.changeTaskStatusById(userId, null, Status.IN_PROGRESS));
        Assert.assertThrows(AbstractException.class, () -> taskService.changeTaskStatusById(userId, USER_TASK1.getId(), null));
        Assert.assertThrows(AbstractException.class, () -> taskService.changeTaskStatusById(userId, NON_EXISTING_TASK_ID, Status.IN_PROGRESS));
        Assert.assertThrows(AbstractException.class, () -> taskService.changeTaskStatusById(NON_EXISTING_USER_ID, USER_TASK1.getId(), Status.IN_PROGRESS));
        @NotNull final Task task = taskService.changeTaskStatusById(userId, USER_TASK1.getId(), Status.IN_PROGRESS);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test
    public void changeProjectStatusByIndex() {
        @NotNull final List<Task> tasks = taskService.findAll(userId);
        final int index = getIndexFromList(tasks, USER_TASK1.getId());
        Assert.assertThrows(AbstractException.class, () -> taskService.changeTaskStatusByIndex(null, index, Status.COMPLETED));
        Assert.assertThrows(AbstractException.class, () -> taskService.changeTaskStatusByIndex(userId, null, Status.COMPLETED));
        Assert.assertThrows(AbstractException.class, () -> taskService.changeTaskStatusByIndex(userId, index, null));
        Assert.assertThrows(AbstractException.class, () -> taskService.changeTaskStatusByIndex(userId, -1, Status.COMPLETED));
        @NotNull final Task task = taskService.changeTaskStatusByIndex(userId, index, Status.COMPLETED);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void set() {
        taskService.removeAll(userId);
        taskService.removeAll(adminId);
        Assert.assertEquals(EMPTY_TASK_LIST, Collections.emptyList());
        taskService.set(USER_TASK_LIST);
        taskService.set(ADMIN_TASK_LIST);
        final List<Task> tasks = taskService.findAll(adminId);
        tasks.forEach(task -> Assert.assertEquals(adminId, task.getUserId()));
        final List<Task> tasks2 = taskService.findAll(userId);
        tasks2.forEach(task -> Assert.assertEquals(userId, task.getUserId()));
    }

    private int getIndexFromList(@NotNull final List<Task> tasks, @NotNull final String id) {
        int index = 0;
        for (Task task : tasks) {
            index++;
            if (id.equals(task.getId())) return index - 1;
        }
        return -1;
    }

    @Test
    public void findAll() {
        taskService.removeAll(userId);
        taskService.set(USER_TASK_LIST);
        final List<Task> tasks = taskService.findAll();
        tasks.forEach(task -> Assert.assertEquals(userId, task.getUserId()));
    }

    @Test
    public void findAllByUserId() {
        Assert.assertThrows(AbstractException.class, () -> taskService.findAll(""));
        Assert.assertEquals(Collections.emptyList(), taskService.findAll(NON_EXISTING_USER_ID));
        final List<Task> tasks = taskService.findAll(userId);
        tasks.forEach(task -> Assert.assertEquals(userId, task.getUserId()));
    }

    @Test
    public void findAllComparator() {
        taskService.removeAll(userId);
        taskService.removeAll(adminId);
        taskService.set(USER_TASK_LIST);
        taskService.set(ADMIN_TASK_LIST);
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        final List<Task> tasks = taskService.findAll(userId, comparator);
        tasks.forEach(task -> Assert.assertEquals(userId, task.getUserId()));
        final List<Task> tasks2 = taskService.findAll(adminId, comparator);
        tasks2.forEach(task -> Assert.assertEquals(adminId, task.getUserId()));
    }

    @Test
    public void findOneById() {
        Assert.assertThrows(AbstractException.class, () -> taskService.findOneById(userId, ""));
        Assert.assertThrows(AbstractException.class, () -> taskService.findOneById(userId, NON_EXISTING_TASK_ID));
        @Nullable final Task task = taskService.findOneById(userId, USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void findOneByUserId() {
        Assert.assertThrows(AbstractException.class, () -> taskService.findOneById("", USER_TASK1.getId()));
        Assert.assertThrows(AbstractException.class, () -> taskService.findOneById(userId, null));
        Assert.assertThrows(AbstractException.class, () -> taskService.findOneById(userId, NON_EXISTING_TASK_ID));
        @Nullable final Task task = taskService.findOneById(userId, USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void findOneByIndex() {
        @NotNull final List<Task> tasks = taskService.findAll(userId);
        final int index = getIndexFromList(tasks, USER_TASK1.getId());
        Assert.assertThrows(AbstractException.class, () -> taskService.findOneByIndex(userId, -1));
        @Nullable final Task task = taskService.findOneByIndex(userId, index);
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void findOneByIndexByUserId() {
        @NotNull final List<Task> tasks = taskService.findAll(userId);
        final int index = getIndexFromList(tasks, USER_TASK1.getId());
        Assert.assertThrows(AbstractException.class, () -> taskService.findOneByIndex(userId, -1));
        Assert.assertThrows(AbstractException.class, () -> taskService.findOneByIndex(null, index));
        @Nullable final Task task = taskService.findOneByIndex(userId, index);
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void existsById() {
        Assert.assertThrows(AbstractException.class, () -> taskService.existsById(userId, null));
        Assert.assertFalse(taskService.existsById(userId, NON_EXISTING_TASK_ID));
        Assert.assertTrue(taskService.existsById(userId, USER_TASK1.getId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertThrows(AbstractException.class, () -> taskService.existsById(userId, null));
        Assert.assertFalse(taskService.existsById(userId, NON_EXISTING_TASK_ID));
        Assert.assertTrue(taskService.existsById(userId, USER_TASK1.getId()));
    }

    @Test
    public void removeAll() {
        taskService.removeAll(userId);
        taskService.removeAll(adminId);
        Assert.assertEquals(0, taskService.getSize(userId));
        Assert.assertEquals(0, taskService.getSize(adminId));
        taskService.set(TASK_LIST);
        Assert.assertNotEquals(0, taskService.getSize(userId));
        Assert.assertNotEquals(0, taskService.getSize(adminId));
    }

    @Test
    public void removeOne() {
        @Nullable final Task task = taskService.add(ADMIN_TASK1);
        Assert.assertNotNull(taskService.findOneById(adminId, ADMIN_TASK1.getId()));
        taskService.remove(adminId, task);
        Assert.assertThrows(AbstractException.class, () -> taskService.findOneById(adminId, ADMIN_TASK1.getId()));
    }

    @Test
    public void removeOneByIdByUserId() {
        Assert.assertThrows(AbstractException.class, () -> taskService.removeOneById(null, ADMIN_TASK2.getId()));
        Assert.assertThrows(AbstractException.class, () -> taskService.removeOneById(adminId, null));
        Assert.assertThrows(AbstractException.class, () -> taskService.removeOneById(userId, NON_EXISTING_TASK_ID));
        @Nullable final Task task = taskService.add(ADMIN_TASK2);
        Assert.assertNotNull(taskService.findOneById(adminId, ADMIN_TASK2.getId()));
        taskService.removeOneById(adminId, task.getId());
        Assert.assertThrows(AbstractException.class, () -> taskService.findOneById(adminId, ADMIN_TASK2.getId()));
    }

    @Test
    public void removeOneByIndex() {
        taskService.add(ADMIN_TASK1);
        Assert.assertThrows(AbstractException.class, () -> taskService.removeOneByIndex(adminId, -1));
        @NotNull final List<Task> tasks = taskService.findAll(adminId);
        final int index = getIndexFromList(tasks, ADMIN_TASK1.getId());
        @Nullable final Task task = taskService.findOneByIndex(adminId, index);
        Assert.assertEquals(ADMIN_TASK1.getId(), task.getId());
        Assert.assertNotNull(task);
        @Nullable final Task task2 = taskService.removeOneByIndex(adminId, index);
        Assert.assertNotNull(task2);
        Assert.assertThrows(AbstractException.class, () -> taskService.findOneById(adminId, ADMIN_TASK1.getId()));
    }

    @Test
    public void removeOneByIndexByUserId() {
        taskService.add(ADMIN_TASK1);
        @NotNull final List<Task> tasks = taskService.findAll(adminId);
        final int index = getIndexFromList(tasks, ADMIN_TASK1.getId());
        Assert.assertThrows(AbstractException.class, () -> taskService.removeOneByIndex(ADMIN_TASK1.getId(), -1));
        Assert.assertThrows(AbstractException.class, () -> taskService.removeOneByIndex(null, index));
        @Nullable final Task task2 = taskService.removeOneByIndex(adminId, index);
        Assert.assertNotNull(task2);
        Assert.assertThrows(AbstractException.class, () -> taskService.findOneById(task2.getId(), task2.getUserId()));
    }

}
