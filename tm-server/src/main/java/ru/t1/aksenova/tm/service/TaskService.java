package ru.t1.aksenova.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.repository.ITaskRepository;
import ru.t1.aksenova.tm.api.service.IConnectionService;
import ru.t1.aksenova.tm.api.service.ITaskService;
import ru.t1.aksenova.tm.comparator.CreatedComparator;
import ru.t1.aksenova.tm.comparator.NameComparator;
import ru.t1.aksenova.tm.comparator.StatusComparator;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.enumerated.TaskSort;
import ru.t1.aksenova.tm.exception.entity.TaskNotFoundException;
import ru.t1.aksenova.tm.exception.field.*;
import ru.t1.aksenova.tm.model.Task;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class TaskService implements ITaskService {

    @NotNull
    private final IConnectionService connectionService;

    public TaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(@Nullable final Task task) {
        if (task == null) throw new TaskNotFoundException();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            repository.add(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(
            @Nullable final String userId,
            @Nullable final Task task
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new TaskNotFoundException();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            repository.addWithUserId(userId, task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @Override
    @NotNull
    @SneakyThrows
    public Collection<Task> set(@NotNull Collection<Task> tasks) {
        if (tasks.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            tasks.forEach(repository::add);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return tasks;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        add(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll() {
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            return repository.findAll();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            return repository.findAllByUserId(userId);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@Nullable final String userId, @Nullable final TaskSort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(
            @Nullable final String userId,
            @Nullable final Comparator<Task> comparator
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        @NotNull List<Task> tasks = Collections.emptyList();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            if (CreatedComparator.INSTANCE.equals(comparator)) tasks = repository.findAllOrderByCreated(userId);
            if (StatusComparator.INSTANCE.equals(comparator)) tasks = repository.findAllOrderByStatus(userId);
            if (NameComparator.INSTANCE.equals(comparator)) tasks = repository.findAllOrderByName(userId);
            return tasks;
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull List<Task> tasks;
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            tasks = repository.findAllByProjectId(userId, projectId);
        }
        if (tasks.size() <= 0) throw new TaskNotFoundException();
        return tasks;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            @Nullable final Task task = repository.findOneById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            return task;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            @Nullable final Task task = repository.findOneByIndex(userId, index);
            if (task == null) throw new TaskNotFoundException();
            return task;
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            repository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            repository.removeAll(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task remove(
            @Nullable final String userId,
            @Nullable final Task task
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new TaskNotFoundException();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            repository.removeOne(userId, task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        remove(userId, task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        @Nullable final Task task = findOneByIndex(userId, index);
        remove(userId, task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            repository.update(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            repository.update(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateProjectIdById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String project_id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(project_id);
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            repository.updateTaskProjectId(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            repository.update(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            repository.update(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            return (repository.findOneById(userId, id) != null);
        }
    }

    @Override
    @SneakyThrows
    public long getSize(@Nullable final String userId) {
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = connection.getMapper(ITaskRepository.class);
            return repository.getSize(userId);
        }
    }

}
