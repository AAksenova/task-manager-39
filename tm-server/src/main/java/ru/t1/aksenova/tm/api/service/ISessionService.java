package ru.t1.aksenova.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.model.Session;

import java.util.Date;
import java.util.List;

public interface ISessionService {

    @NotNull
    Session add(@Nullable Session session);

    @NotNull
    Session create(@Nullable String userId, @Nullable String role);

    @NotNull
    List<Session> findAll(@Nullable String userId);

    @Nullable
    Session findOneById(@Nullable String userId, @Nullable String id);

    void clear();

    void removeAll(@Nullable String userId);

    @Nullable
    Session remove(@Nullable Session session);

    @Nullable
    Session removeOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    @SneakyThrows
    Session updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String role,
            @Nullable Date date
    );

    boolean existsById(@Nullable String userId, @Nullable String id);

    long getSize(@Nullable String userId);
}
