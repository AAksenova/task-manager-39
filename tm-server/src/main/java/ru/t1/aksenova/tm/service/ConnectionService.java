package ru.t1.aksenova.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.api.repository.IProjectRepository;
import ru.t1.aksenova.tm.api.repository.ISessionRepository;
import ru.t1.aksenova.tm.api.repository.ITaskRepository;
import ru.t1.aksenova.tm.api.repository.IUserRepository;
import ru.t1.aksenova.tm.api.service.IConnectionService;
import ru.t1.aksenova.tm.api.service.IDatabaseProperty;

import javax.sql.DataSource;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IDatabaseProperty databaseProperty;

    @NotNull
    private final SqlSessionFactory sqlSessionFactory;

    public ConnectionService(@NotNull final IDatabaseProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
        this.sqlSessionFactory = getSqlSessionFactory();
    }

    @NotNull
    @Override
    @SneakyThrows
    public SqlSession getConnection() {
        return sqlSessionFactory.openSession();
    }

    @NotNull
    @SneakyThrows
    private SqlSessionFactory getSqlSessionFactory() {
        @NotNull final String user = databaseProperty.getDatabaseUser();
        @NotNull final String password = databaseProperty.getDatabasePassword();
        @NotNull final String url = databaseProperty.getDatabaseConnection();
        @NotNull final String driver = databaseProperty.getDatabaseDriver();
        @NotNull final DataSource dataSource = new PooledDataSource(driver, url, user, password);
        @NotNull final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("tm", transactionFactory, dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

}
